import { combineReducers } from 'redux';
import main from './main';
import favorites from './favorites';

export default combineReducers({
  main,
  favorites,
});
