import * as actionCreators from './action-creators';

export const fetchMainData = (category) => async (dispatch) => dispatch(actionCreators.fetchMainData(category));
