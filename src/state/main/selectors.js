export const getMainData = (state) => {
  if (!state.main.data) return [];
  return state.main.data;
};

export const isPending = (state) => state.main.pending;
