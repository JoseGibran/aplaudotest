import * as types from './action-types';

const intialState = {
  data: [],
  pending: false,
  errorMessage: '',
};

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case types.MAIN_PENDING:
      return {
        ...state,
        pending: true,
        errorMessage: '',
      };
    case types.MAIN_FULLFILLED:
      return {
        data: action.payload.data,
        pending: false,
        errorMessage: '',
      };
    case types.MAIN_REJECTED:
      return {
        ...state,
        pending: false,
        errorMessage: action.payload.message,
      };
    default:
      return state;
  }
};

export default reducer;
