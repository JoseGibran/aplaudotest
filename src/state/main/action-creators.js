import { RSAA } from 'redux-api-middleware';
import Config from 'react-native-config';
import * as types from './action-types';

export const fetchMainData = (category) => ({
  [RSAA]: {
    method: 'GET',
    headers: {
      Accept: 'application/vnd.api+json',
      'Content-Type': 'application/vnd.api+json',
    },
    types: [types.MAIN_PENDING, types.MAIN_FULLFILLED, types.MAIN_REJECTED],
    endpoint: `${Config.API_SERVER}/anime?filter[categories]=${category}`,
  },
});
