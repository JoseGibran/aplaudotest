export const getFavorites = (state) => {
  if (!state.favorites) return [];
  return state.favorites;
};
