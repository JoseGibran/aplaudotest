import * as types from './action-types';

export const addFavorite = (item) => ({
  type: types.FAVORITES_ADD,
  payload: item,
});

export const deleteFavorite = (item) => ({
  type: types.FAVORITES_DELETE,
  payload: item,
});
