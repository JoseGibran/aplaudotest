import * as types from './action-types';

const intialState = [];

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case types.FAVORITES_ADD:
      return [...state, action.payload];
    case types.FAVORITES_DELETE:
      return [...state.filter((x) => x.id !== action.payload.id)];
    default:
      return state;
  }
};

export default reducer;
