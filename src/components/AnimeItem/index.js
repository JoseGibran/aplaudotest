/* eslint-disable */
import React from 'react';
import {
  ImageBackground, Text, View, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import styles from './styles';
import { colors } from '../../constants';

const AnimeItem = (props) => {
  const { canonicalTitle, posterImage } = props.item.attributes;

  return (
    <TouchableOpacity onPress={props.onPress}>
      <ImageBackground style={styles.image} width="100%" height="100%" source={{ uri: posterImage.medium }}>
        <View style={styles.shadowBox} />
        <Text style={styles.text}>{canonicalTitle}</Text>

        <TouchableOpacity style={styles.favIcon} onPress={props.isFav ? props.unFav : props.onFav}>
          <Icon name={props.isFav ? 'star' : 'star-o'} size={30} color={colors.TEXT_COLOR} />
        </TouchableOpacity>
      </ImageBackground>

    </TouchableOpacity>

  );
};
/* eslint-enable */
AnimeItem.defaultProps = {
  item: {
    attributes: {},
  },
  isFav: false,
  onPress: () => {},
  onFav: () => {},
  unFav: () => {},
};
AnimeItem.prototypes = {
  item: PropTypes.object,
  isFav: PropTypes.bool,
  onPress: PropTypes.func,
  onFav: PropTypes.func,
  unFav: PropTypes.func,
};

export default AnimeItem;
