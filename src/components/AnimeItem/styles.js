import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 300,
    justifyContent: 'flex-end',
  },
  shadowBox: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  text: {
    color: colors.TEXT_COLOR,
    fontSize: 24,
    fontWeight: 'bold',
    paddingBottom: 10,
    marginLeft: 20,
  },
  favIcon: {
    position: 'absolute',
    padding: 10,
    top: 0,
    left: 0,
  },
});

export default styles;
