import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

const styles = StyleSheet.create({
  shadeBox: {
    width: 80,
    height: 80,
    backgroundColor: colors.TEXT_COLOR,
    alignContent: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    opacity: 0.5,
  },
  activityIndicator: { alignSelf: 'center', width: 80, height: 80 },
});

export default styles;
