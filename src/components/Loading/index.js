import { ActivityIndicator, View } from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import { colors } from '../../constants';
import styles from './styles';

class Loading extends PureComponent {
  onModalShow = () => {
    const { onShow } = this.props;
    if (onShow !== undefined) {
      onShow();
    }
  };

  onModalHide = () => {
    const { onHide } = this.props;
    if (onHide !== undefined) {
      onHide();
    }
  };

  renderActivityIndicator = (isMinimal) => {
    if (!isMinimal) {
      return <ActivityIndicator size="large" color={colors.PRIMARY_COLOR} />;
    }
    return (
      <View>
        <View style={styles.shadeBox}>
          <ActivityIndicator size="large" color={colors.BACKGROUND_COLOR} style={styles.activityIndicator} />
        </View>
      </View>
    );
  };

  render() {
    const { showModal, isMinimal = false } = this.props;
    return (
      <Modal
        isVisible={showModal}
        backdropColor={isMinimal ? colors.BACKGROUND_COLOR : colors.TEXT_COLOR}
        backdropOpacity={isMinimal ? 0.0 : 0.7}
        animationIn={isMinimal ? 'fadeIn' : 'slideInUp'}
        animationOut={isMinimal ? 'fadeOut' : 'slideOutDown'}
        onModalShow={this.onModalShow}
        onModalHide={this.onModalHide}
      >
        {this.renderActivityIndicator(isMinimal)}
      </Modal>
    );
  }
}
Loading.propTypes = {
  showModal: PropTypes.bool,
  isMinimal: PropTypes.bool,
  onShow: PropTypes.func,
  onHide: PropTypes.func,
};
Loading.defaultProps = {
  showModal: false,
  isMinimal: false,
  onShow: () => {},
  onHide: () => {},
};

export default Loading;
