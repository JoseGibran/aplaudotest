import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginRight: 20,
    marginLeft: 20,
    marginTop: 30,
    alignItems: 'center',
  },
  input: {
    marginLeft: 10,
    backgroundColor: colors.BACKGROUND_COLOR,
    height: 60,
    flex: 1,
    color: colors.TEXT_COLOR,
    fontSize: 20,
  },
  iconContainer: {
    height: 60,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },

});
