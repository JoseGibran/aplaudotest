import React from 'react';
import PropTypes from 'prop-types';
import { TextInput, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import styles from './styles';
import { colors } from '../../constants';

class SearchBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  render() {
    const { onChangeText } = this.props;
    const { text } = this.state;
    return (
      <View style={styles.container}>
        <Icon name="search" size={30} color={colors.TEXT_COLOR} />
        <TextInput
          value={text}
          onChangeText={(textChanged) => {
            this.setState({ text: textChanged });
            onChangeText(textChanged);
          }}
          style={styles.input}
          placeholder="search"
          placeholderTextColor={colors.TEXT_COLOR}
        />
        <TouchableOpacity
          onPress={() => {
            this.setState({ text: '' });
            onChangeText('');
          }}
          style={styles.iconContainer}
        >
          <Icon onChangeText={onChangeText} name="close" size={30} color={colors.TEXT_COLOR} />
        </TouchableOpacity>
      </View>
    );
  }
}
SearchBar.propTypes = {
  onChangeText: PropTypes.func,
};
SearchBar.defaultProps = {
  onChangeText: (text) => `${text}`,
};

export default SearchBar;
