import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

export default StyleSheet.create({

  container: {
    backgroundColor: colors.BACKGROUND_COLOR,
    flex: 1,
  },

  separator: {
    width: 20,
  },
  sectionHeaderContainer: {
    paddingVertical: 20,
    backgroundColor: colors.BACKGROUND_COLOR,
    paddingLeft: 20,
  },
  sectionHeader: {
    color: colors.TEXT_COLOR,
    fontSize: 24,

  },

});
