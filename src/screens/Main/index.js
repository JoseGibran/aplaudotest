import React from 'react';
import {
  SafeAreaView, SectionList, FlatList, Text, View, Alert,
} from 'react-native';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { SearchBar, Loading, AnimeItem } from '../../components';

import { fetchMainData } from '../../state/main/actions';

import styles from './styles';
import { addFavorite, deleteFavorite } from '../../state/favorites/actions';
import { getFavorites } from '../../state/favorites/selectors';

class Main extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      filter: '',
      data: [],
      pending: false,
    };
  }

  async componentDidMount() {
    const { fetchMainData } = this.props;
    this.setState({ pending: true });
    const adventureResult = await fetchMainData('adventure');
    const fantasyResult = await fetchMainData('fantasy');
    const romanceResult = await fetchMainData('romance');
    this.setState({ pending: false });

    this.isServerAvailable();

    const data = [
      {
        title: 'Adventure',
        data: adventureResult.payload.data || [],
      },
      {
        title: 'Fantasy',
        data: fantasyResult.payload.data || [],
      },
      {
        title: 'Romance',
        data: romanceResult.payload.data || [],
      },
    ];

    this.setState({ data });
  }

  isFav(item) {
    const { favorites } = this.props;
    return favorites.find((x) => x.id === item.id);
  }

  isServerAvailable() {
    const timeout = new Promise((resolve, reject) => {
      setTimeout(reject, 300, 'Request timed out');
    });

    const request = fetch('https://kitsu.io/api/edge');

    return Promise
      .race([timeout, request])
      .then(() => {})
      .catch(() => Alert.alert('Server unavailable'));
  }

  render() {
    const { pending, data, filter } = this.state;
    const { navigation, addFavorite, deleteFavorite } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <SearchBar onChangeText={(text) => this.setState({ filter: text })} />

        <SectionList
          sections={data || []}
          keyExtractor={(item) => item.id}
          renderItem={({ section, index }) => index === 0 && (
          <FlatList
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            data={(section.data || []).filter((x) => x.attributes.canonicalTitle.indexOf(filter) >= 0)}
            horizontal
            renderItem={({ item }) => (
              <AnimeItem
                onFav={() => addFavorite(item)}
                onPress={() => navigation.navigate('Single', { item })}
                item={item}
                unFav={() => deleteFavorite(item)}
                isFav={this.isFav(item)}
              />
            )}
          />
          )}
          renderSectionHeader={({ section: { title } }) => (
            <View style={styles.sectionHeaderContainer}>
              <Text style={styles.sectionHeader}>{title}</Text>
            </View>

          )}
        />

        <Loading showModal={pending} />
      </SafeAreaView>
    );
  }
}
Main.propTypes = {
  favorites: PropTypes.array,
  fetchMainData: PropTypes.func,
  addFavorite: PropTypes.func,
  deleteFavorite: PropTypes.func,
};

Main.defaultProps = {
  favorites: [],
  fetchMainData: () => {},
  addFavorite: () => {},
  deleteFavorite: () => {},
};

const mapDispatchToProps = (dispatch) => ({
  fetchMainData: (category) => dispatch(fetchMainData(category)),
  addFavorite: (item) => dispatch(addFavorite(item)),
  deleteFavorite: (item) => dispatch(deleteFavorite(item)),
});

const mapStateToProps = (state) => ({
  favorites: getFavorites(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
