import React from 'react';
import { SafeAreaView, FlatList, View } from 'react-native';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { AnimeItem } from '../../components';
import { getFavorites } from '../../state/favorites/selectors';

import styles from './styles';
import { deleteFavorite } from '../../state/favorites/actions';

class Favorites extends React.PureComponent {
  render() {
    const { data, deleteFavorite } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          contentContainerStyle={{ alignItems: 'center', paddingVertical: 20 }}
          data={data}
          renderItem={({ item }) => <AnimeItem item={item} isFav unFav={() => deleteFavorite(item)} />}
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      </SafeAreaView>
    );
  }
}
Favorites.propTypes = {
  data: PropTypes.array,
};

Favorites.defaultProps = {
  data: [],
};

const mapDispatchToProps = (dispatch) => ({
  deleteFavorite: (item) => dispatch(deleteFavorite(item)),
});

const mapstateToProps = (state) => ({
  data: getFavorites(state),
});

export default connect(mapstateToProps, mapDispatchToProps)(Favorites);
