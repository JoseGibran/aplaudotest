import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

export default StyleSheet.create({

  container: {
    backgroundColor: colors.BACKGROUND_COLOR,
    flex: 1,
  },

  separator: {
    height: 20,
  },

});
