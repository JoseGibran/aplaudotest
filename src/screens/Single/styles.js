import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.BACKGROUND_COLOR,
    height: '100%',
  },
  header: {
    flexDirection: 'row',
    marginBottom: 30,
  },
  info: {
    marginLeft: 20,
  },
  image: {
    width: 150,
    height: 200,
  },
  textLabel: {
    color: colors.TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 5,
  },
  textValue: {
    color: colors.TEXT_COLOR,
    fontSize: 16,
  },
  synopsis: {
    marginTop: 30,
  },
});
