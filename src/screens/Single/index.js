import React from 'react';
import {
  View, Text, Image, ScrollView, SafeAreaView, Linking,
} from 'react-native';
import moment from 'moment';

import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';

class Single extends React.PureComponent {
  render() {
    // eslint-disable-next-line react/destructuring-assignment
    const { attributes } = this.props.route.params.item;
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView contentContainerStyle={{ paddingVertical: 30, paddingHorizontal: 20 }}>
          <View style={styles.header}>
            <Image style={styles.image} source={{ uri: attributes.posterImage.large }} />
            <View style={styles.info}>
              <Text style={styles.textLabel}>Main Title</Text>
              <Text style={styles.textValue}>{attributes.titles.en || attributes.titles.en_jp || attributes.titles.ja_jp }</Text>
              <Text style={styles.textLabel}>Canonical Title</Text>
              <Text style={styles.textValue}>{attributes.canonicalTitle}</Text>
              <Text style={styles.textLabel}>Type</Text>
              <Text style={styles.textValue}>{attributes.showType}</Text>
              <Text style={styles.textLabel}>Year</Text>
              <Text style={styles.textValue}>{ moment(attributes.startDate, 'YYYY-MM-DD').format('YYYY') }</Text>
            </View>
          </View>
          <Text style={styles.textLabel}>Genres</Text>
          <Text style={styles.textValue}>Genres list</Text>

          <View style={styles.hbox}>
            <View>
              <Text style={styles.textLabel}>Average Rating</Text>
              <Text style={styles.textValue}>{attributes.popularityRank}</Text>
            </View>
            <View>
              <Text style={styles.textLabel}>Age Rating</Text>
              <Text style={styles.textValue}>{attributes.ageRating}</Text>
            </View>
          </View>
          <View style={styles.hbox}>
            <View>
              <Text style={styles.textLabel}>Episode Duration</Text>
              <Text style={styles.textValue}>{attributes.episodelenght}</Text>
            </View>
            <View>
              <Text style={styles.textLabel}>Airing Status</Text>
              <Text style={styles.textValue}>Status</Text>
            </View>
          </View>

          <View style={styles.synopsis}>
            <Text style={styles.textLabel}>Synopsis</Text>
            <Text style={styles.textValue}>{attributes.synopsis}</Text>
          </View>

          {attributes.youtubeVideoId ? (
            <TouchableOpacity onPress={() => Linking.openURL(`https://www.youtube.com/watch?v=${attributes.youtubeVideoId}`)}>
              <Text style={styles.textLabel}>Open Youtube Video</Text>
            </TouchableOpacity>
          ) : null}

        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Single;
