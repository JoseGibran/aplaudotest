module.exports = {
  env: {
    browser: true,
    es2020: true,
    jest: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/jsx-filename-extension': [0],
    'import/prefer-default-export': [0],
    'import/no-named-as-default': [0],
    'import/no-named-as-default-member': [0],
    'react/prop-types': [0],
    'max-len': [0],
    'no-shadow': [0],
    'class-methods-use-this': [0],
    'react/forbid-prop-types': [0],
  },
};
