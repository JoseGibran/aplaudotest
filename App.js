/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Provider } from 'react-redux';

import Main from './src/screens/Main';
import Single from './src/screens/Single';
import Favorites from './src/screens/Favorites';

import configureStore from './src/state/store';

const { store, persistor } = configureStore();

const Stack = createStackNavigator();

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Main">
          <Stack.Screen
            name="Main"
            component={Main}
            options={({ navigation }) => ({
              headerRight: () => (
                <TouchableOpacity onPress={() => navigation.navigate('Favorites')}>
                  <Text style={{ marginRight: 20 }}>FAVS</Text>
                </TouchableOpacity>
              ),
            })}
          />
          <Stack.Screen name="Single" component={Single} />
          <Stack.Screen name="Favorites" component={Favorites} />
        </Stack.Navigator>
      </NavigationContainer>
    </PersistGate>
  </Provider>
);
export default App;
